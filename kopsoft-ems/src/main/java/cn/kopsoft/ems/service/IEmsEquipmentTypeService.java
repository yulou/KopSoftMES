package cn.kopsoft.ems.service;

import java.util.List;
import cn.kopsoft.ems.domain.EmsEquipmentType;

/**
 * 设备类型Service接口
 * 
 * @author ruoyi
 * @date 2022-10-17
 */
public interface IEmsEquipmentTypeService 
{
    /**
     * 查询设备类型
     * 
     * @param typeId 设备类型主键
     * @return 设备类型
     */
    public EmsEquipmentType selectEmsEquipmentTypeByTypeId(String typeId);

    /**
     * 查询设备类型列表
     * 
     * @param emsEquipmentType 设备类型
     * @return 设备类型集合
     */
    public List<EmsEquipmentType> selectEmsEquipmentTypeList(EmsEquipmentType emsEquipmentType);

    /**
     * 新增设备类型
     * 
     * @param emsEquipmentType 设备类型
     * @return 结果
     */
    public int insertEmsEquipmentType(EmsEquipmentType emsEquipmentType);

    /**
     * 修改设备类型
     * 
     * @param emsEquipmentType 设备类型
     * @return 结果
     */
    public int updateEmsEquipmentType(EmsEquipmentType emsEquipmentType);

    /**
     * 批量删除设备类型
     * 
     * @param typeIds 需要删除的设备类型主键集合
     * @return 结果
     */
    public int deleteEmsEquipmentTypeByTypeIds(String[] typeIds);

    /**
     * 删除设备类型信息
     * 
     * @param typeId 设备类型主键
     * @return 结果
     */
    public int deleteEmsEquipmentTypeByTypeId(String typeId);
}
