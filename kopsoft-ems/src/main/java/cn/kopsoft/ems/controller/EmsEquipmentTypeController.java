package cn.kopsoft.ems.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import cn.kopsoft.ems.domain.EmsEquipmentType;
import cn.kopsoft.ems.service.IEmsEquipmentTypeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 设备类型Controller
 * 
 * @author ruoyi
 * @date 2022-10-17
 */
@RestController
@RequestMapping("/ems/EquipmentType")
public class EmsEquipmentTypeController extends BaseController
{
    @Autowired
    private IEmsEquipmentTypeService emsEquipmentTypeService;

    /**
     * 查询设备类型列表
     */
    @PreAuthorize("@ss.hasPermi('ems:EquipmentType:list')")
    @GetMapping("/list")
    public TableDataInfo list(EmsEquipmentType emsEquipmentType)
    {
        startPage();
        List<EmsEquipmentType> list = emsEquipmentTypeService.selectEmsEquipmentTypeList(emsEquipmentType);
        return getDataTable(list);
    }

    /**
     * 导出设备类型列表
     */
    @PreAuthorize("@ss.hasPermi('ems:EquipmentType:export')")
    @Log(title = "设备类型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, EmsEquipmentType emsEquipmentType)
    {
        List<EmsEquipmentType> list = emsEquipmentTypeService.selectEmsEquipmentTypeList(emsEquipmentType);
        ExcelUtil<EmsEquipmentType> util = new ExcelUtil<EmsEquipmentType>(EmsEquipmentType.class);
        util.exportExcel(response, list, "设备类型数据");
    }

    /**
     * 获取设备类型详细信息
     */
    @PreAuthorize("@ss.hasPermi('ems:EquipmentType:query')")
    @GetMapping(value = "/{typeId}")
    public AjaxResult getInfo(@PathVariable("typeId") String typeId)
    {
        return AjaxResult.success(emsEquipmentTypeService.selectEmsEquipmentTypeByTypeId(typeId));
    }

    /**
     * 新增设备类型
     */
    @PreAuthorize("@ss.hasPermi('ems:EquipmentType:add')")
    @Log(title = "设备类型", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EmsEquipmentType emsEquipmentType)
    {
        return toAjax(emsEquipmentTypeService.insertEmsEquipmentType(emsEquipmentType));
    }

    /**
     * 修改设备类型
     */
    @PreAuthorize("@ss.hasPermi('ems:EquipmentType:edit')")
    @Log(title = "设备类型", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EmsEquipmentType emsEquipmentType)
    {
        return toAjax(emsEquipmentTypeService.updateEmsEquipmentType(emsEquipmentType));
    }

    /**
     * 删除设备类型
     */
    @PreAuthorize("@ss.hasPermi('ems:EquipmentType:remove')")
    @Log(title = "设备类型", businessType = BusinessType.DELETE)
	@DeleteMapping("/{typeIds}")
    public AjaxResult remove(@PathVariable String[] typeIds)
    {
        return toAjax(emsEquipmentTypeService.deleteEmsEquipmentTypeByTypeIds(typeIds));
    }
}
